package services.agent

import akka.actor.{ReceiveTimeout, Props}
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import org.joda.time.DateTime
import org.scalatest.mock.MockitoSugar
import services.request.{AgentLogout, BaseRequest}
import services.{ActorFactory, XucEventBus}
import xivo.events.AgentState.AgentLoggedOut
import xivo.xuc.api.{RequestTimeout, RequestSuccess}

class LogoutProcessorSpec extends TestKitSpec("LogoutProcessorSpec") with MockitoSugar {

  val eventBus = mock[XucEventBus]
  val configManager = TestProbe()

  trait TestActorFactory extends ActorFactory {
    override val configManagerURI = (configManager.ref.path).toString
  }


  class Helper {
    def actor = {
      val a = TestActorRef[LogoutProcessor](Props(new LogoutProcessor(eventBus) with TestActorFactory))
      (a, a.underlyingActor)
    }
  }

  "A LogoutProcessor" should {
    "process Logout requests" in new Helper {
      val (ref, _) = actor
      ref ! LogoutAgent("1010")
      configManager.expectMsg(BaseRequest(ref, AgentLogout("1010")))
    }

    "process AgentLoggedOut event" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      processor.expectingResult(requester.ref, "1010")(AgentLoggedOut(11L, new DateTime(), "1010", List(1)))
      requester.expectMsgClass(classOf[RequestSuccess])
    }

    "process ReceiveTimeout event in receive state" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      requester watch(ref)
      ref ! ReceiveTimeout
      requester expectTerminated(ref)
    }

    "process ReceiveTimeout event in expectingResult state" in new Helper {
      val (ref, processor) = actor
      val requester = TestProbe()
      processor.expectingResult(requester.ref, "1010")(ReceiveTimeout)
      requester.expectMsgClass(classOf[RequestTimeout])
    }
  }
}

