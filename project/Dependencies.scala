import sbt._
import play.Play.autoImport._

object Version {
  val akka      = "2.3.4"
  val scalaTest = "2.2.4"
  val xivojavactilib = "0.0.33.0"
  val mockito = "1.10.19"
  val xucstats = "2.3.2"
  val xucami = "1.10.4"
  val playauthentication = "1.9.0"
  val metrics = "3.1.0"
  val dbunit = "2.4.7"
  val scalatestplay = "1.2.0"
}

object Library {
  val xivojavactilib     = "org.xivo"               % "xivo-javactilib"     % Version.xivojavactilib
  val playauthentication = "xivo"                  %% "play-authentication" % Version.playauthentication
  val xucstats           = "xivo"                  %% "xucstats"            % Version.xucstats changing()
  val xucami             = "xivo"                  %% "xucami"              % Version.xucami changing()
  val metrics            = "io.dropwizard.metrics"  % "metrics-core"        % Version.metrics
  val akkaTestkit        = "com.typesafe.akka"     %% "akka-testkit"        % Version.akka
  val scalaTest          = "org.scalatest"         %% "scalatest"           % Version.scalaTest
  val mockito            = "org.mockito"            % "mockito-all"         % Version.mockito
  val dbunit             = "org.dbunit"             % "dbunit"              % Version.dbunit
  val scalatestplay      = "org.scalatestplus"     %% "play"                % Version.scalatestplay
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us"),
    ("Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/")

  )

  val runDep = run(
    xivojavactilib,
    xucstats,
    xucami,
    playauthentication,
    javaWs,
    metrics
  )

  val testDep = test(
    scalaTest,
    akkaTestkit,
    mockito,
    dbunit,
    scalatestplay
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
