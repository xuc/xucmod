package models

import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat}
import play.api.libs.functional.syntax._
import play.api.libs.json._

sealed trait XivoAuth

/*
 * compatible with XiVO auth swagger specification v0.1
 */

case class CreateTokenParameters(backend: String, expires: Option[Integer]) extends XivoAuth

case class TokenRequest(backend: String, expiration: Long) extends XivoAuth
object TokenRequest {
  implicit val tokenRequestWrites = new Writes[TokenRequest] {
    def writes(tReq: TokenRequest) = Json.obj(
      "backend" -> tReq.backend,
      "expiration" -> tReq.expiration
    )
  }
}

case class Token(token: String, expiresAt: DateTime, issuedAt: DateTime, authId: String, xivoUserUuid: String) extends XivoAuth
object Token {
  val fmt = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
  implicit val TokenRead: Reads[Token] = (
    (JsPath \ "data" \ "token").read[String] and
      (JsPath \ "data" \ "expires_at").read[String].map(fmt.parseDateTime(_)) and
      (JsPath \ "data" \ "issued_at").read[String].map(fmt.parseDateTime(_)) and
      (JsPath \ "data" \ "auth_id").read[String] and
      (JsPath \ "data" \ "xivo_user_uuid").read[String]
    )(Token.apply _)
}

case class Error(message: String, status: Integer)
