package system

import com.codahale.metrics.{Slf4jReporter, SharedMetricRegistries}
import com.codahale.metrics.jvm.{ThreadStatesGaugeSet, MemoryUsageGaugeSet}
import play.api.Logger
import akka.actor.{Actor, ActorLogging, Props, Terminated}
import services.agent.{AgentDeviceManager, AgentConfig}
import services._
import services.config.{ConfigManager, ConfigDispatcher, ConfigRepository}
import models.XucUser
import xivo.ami.AmiBusConnector
import xivo.xuc.XucConfig
import java.util.concurrent.TimeUnit


object MainRunner {
  case class StartUser(user:XucUser)

  val MainRunnerPath = s"/user/${ActorFactory.MainRunnerId}"

  def props = Props(new MainRunner() with ProductionActorFactory)
}

class MainRunner extends Actor with ActorLogging {
  this: ActorFactory =>

  log.info(s"Xuc Main runner started ................$self")

  if (XucConfig.EventUser!= "") startAllActors()

  startTechMetrics

  def receive = {
    case Terminated(actor) =>
      log.error(s"$actor stopped")
    case _ =>
  }

  private def startAllActors() = {
    val publisher = context.actorOf(StatusPublish.props, ActorFactory.StatusPublishId)
    context.watch(publisher)
    val agentManager = getAgentManager
    val lineManager = getLineManager
    val configRepository = ConfigRepository.repo
    val configDispatcher = context.actorOf(ConfigDispatcher.props(configRepository, lineManager, agentManager), ActorFactory.ConfigDispatcherId)
    context.actorOf(AmiBusConnector.props(configRepository, agentManager, lineManager, configDispatcher), ActorFactory.AmiBusConnectorId)
    context.watch(configDispatcher)
    val configManager = context.actorOf(ConfigManager.props(XucUser(XucConfig.EventUser, XucConfig.DefaultPassword), agentManager), ActorFactory.ConfigManagerId)
    context.watch(configManager)
    context.watch(context.actorOf(AgentConfig.props,ActorFactory.AgentConfigId))
    val agentDeviceManager = context.actorOf(AgentDeviceManager.props, ActorFactory.AgentDeviceManagerId)
    val callHistoryManager = context.actorOf(CallHistoryManager.props(XucConfig.recordingHost, XucConfig.recordingPort,
      XucConfig.recordingLogin, XucConfig.recordingPassword), ActorFactory.CallHistoryManagerId)
  }

  private def startTechMetrics {
    val registry = SharedMetricRegistries.getOrCreate(XucConfig.metricsRegistryName)
    if (XucConfig.metricsRegistryJVM) {
      log.info("Activating JVM metrics")
      registry.registerAll(new MemoryUsageGaugeSet())
      registry.registerAll(new ThreadStatesGaugeSet())
    }

    if (XucConfig.metricsLogReporter) {
      log.info(s"Activating logReporter with period: ${XucConfig.metricsLogReporterPeriod} minutes")
      val reporter= Slf4jReporter.forRegistry(registry)
        .outputTo(Logger.logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(XucConfig.metricsLogReporterPeriod, TimeUnit.MINUTES)
    }
  }
}
