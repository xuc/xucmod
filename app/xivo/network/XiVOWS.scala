package xivo.network

import play.api.libs.json.JsValue
import play.api.libs.json.Json
import play.api.libs.ws._
import xivo.xuc.XucConfig
import play.api.Play.current

trait XiVOWSdef {
  def getWsUrl(resource: String): String
  def withWS(resource: String): WSRequestHolder
  def post(host: String, uri: String, payload: JsValue, user: String, password: String,
           headers: Map[String, String] = Map(), port: Option[String] = None): WSRequestHolder
}

object XiVOWS extends XiVOWSdef {

  val ReqTimeout = 10000

  def getWsUrl(resource: String) = s"https://${XucConfig.XivoWs_host}:${XucConfig.XivoWs_port}/1.1/${resource}"

  def withWS(resource: String): WSRequestHolder = {
    WS.url(getWsUrl(resource))
      .withHeaders(("Content-Type", "application/json"), ("Accept", "application/json"))
      .withAuth(XucConfig.XivoWs_wsUser, XucConfig.XivoWs_wsPwd, WSAuthScheme.DIGEST)
      .withRequestTimeout(ReqTimeout)
  }

  def post(host: String, uri: String, payload: JsValue, user: String, password: String,
           headers: Map[String, String] = Map(), port: Option[String] = None): WSRequestHolder = {

    var request = WS.url(s"${XucConfig.defaultProtocol}://$host:${port.getOrElse("")}/$uri")
    headers.foreach( header => request = request.withHeaders(header))
    request.withAuth(user, password, XucConfig.defaultWSAuthScheme)
    request.withBody(payload)
    request.withMethod("POST")
    request
  }

}
